const wx_server_sdk = require('wx-server-sdk');

wx_server_sdk.init({
    env: 'direshui-6gl2m2r1275bd635'
});
const db = wx_server_sdk.database();
const dbCommand = db.command;

var nowTime = getBeijingTime();

async function funcMain(e) {
    console.log('当前时间', nowTime);

    //后退到整点
    var nowHourTime = new Date();
    nowHourTime.setMinutes(0);
    nowHourTime.setSeconds(0);
    nowHourTime.setMilliseconds(0);

    nowTime = getBeijingTime(nowHourTime.getTime());

    console.log('整点', nowTime);

    var lastHourTime = getBeijingTime(nowTime.ts - 60 * 60 * 1000);

    console.log('上一个整点', lastHourTime); //1600585210

    //开采井
    var dev_start = await db.collection('nowData').where({

        wellName: '刘庄开采井',
        ts: db.command.and([

            { ts: db.command.gte(lastHourTime.ts / 1000) },
            { ts: db.command.lte(nowTime.ts / 1000) },
        ])

    }).orderBy('ts', 'asc').limit(1).get();

    // console.log('查询开采井起始数据', dev_start);

    if (dev_start.errMsg == 'collection.get:ok' && dev_start.data.length) {
        dev_start = dev_start.data[0].rightAccuFlow;
    } else {
        console.log('查询开采井起始数据出错');
        return;
    }



    var dev_end = await db.collection('nowData').where({

        wellName: '刘庄开采井',
        ts: db.command.and([

            { ts: db.command.gte(lastHourTime.ts / 1000) },
            { ts: db.command.lte(nowTime.ts / 1000) },
        ])

    }).orderBy('ts', 'desc').limit(1).get();

    // console.log('查询开采井结束数据', dev_end);


    if (dev_end.errMsg == 'collection.get:ok' && dev_end.data.length) {
        dev_end = dev_end.data[0].rightAccuFlow;
    } else {
        console.log('查询开采井结束数据出错');
        return;
    }

    var devCount = dev_end - dev_start;
    console.log('上小时开采量', devCount);

    //回灌井
    var bak_start = await db.collection('nowData').where({

        wellName: '刘庄回灌井',
        ts: db.command.and([

            { ts: db.command.gte(lastHourTime.ts / 1000) },
            { ts: db.command.lte(nowTime.ts / 1000) },
        ])

    }).orderBy('ts', 'asc').limit(1).get();

    // console.log('查询回灌井起始数据', bak_start);


    if (bak_start.errMsg == 'collection.get:ok' && bak_start.data.length) {
        bak_start = bak_start.data[0].rightAccuFlow;
    } else {
        console.log('查询回灌井起始数据出错');
        return;
    }

    var bak_end = await db.collection('nowData').where({

        wellName: '刘庄回灌井',
        ts: db.command.and([

            { ts: db.command.gte(lastHourTime.ts / 1000) },
            { ts: db.command.lte(nowTime.ts / 1000) },
        ])

    }).orderBy('ts', 'desc').limit(1).get();

    // console.log('查询回灌井结束数据', bak_end);



    if (bak_end.errMsg == 'collection.get:ok' && bak_end.data.length) {
        bak_end = bak_end.data[0].rightAccuFlow;
    } else {
        console.log('查询回灌井结束数据出错');
        return;
    }


    var bakCount = bak_end - bak_start;
    console.log('上小时回灌量', bakCount);

    var bakRate = Math.floor(bakCount / devCount * 1000) / 10;
    console.log('回灌率', bakRate);


    var saveToHour = {
        startTs: lastHourTime.ts,
        startTime: lastHourTime.time,

        endTs: nowTime.ts,
        endTime: nowTime.time,

        devWellName: '刘庄开采井',
        bakWellName: '刘庄回灌井',

        devStart: dev_start,
        devEnd: dev_end,

        bakStart: bak_start,
        bakEnd: bak_end,

        devCount,
        bakCount,
        bakRate,

    }

    var saveResult= await db.collection('hour').add({
    	data:saveToHour
    })

    console.log('保存到hour数据库的情况',saveResult);
    return;
}


function getNowTimestamp() {
    return new Date().getTime();
}

function getBeijingTime(localTs) {
    var time_ts = localTs;
    if (typeof(localTs) != 'number' || localTs == 0) {
        time_ts = getNowTimestamp();
    }

    var time = tsToDateStr(time_ts);

    return {
        ts: time_ts,
        time,
    };
}

function tsToDateStr(ts_para) {
    //2020-06-14 20:49:59
    //未在此弥补时差
    var time_ts = new Date(ts_para + 8 * 3600000);

    // console.log('用于制作中文的ts', time_ts.getTime());

    var time_str = time_ts.getFullYear() + '-';

    if ((time_ts.getMonth() + 1) < 10) {
        time_str += '0';
    }
    time_str += '' + (time_ts.getMonth() + 1) + '-';


    if (time_ts.getDate() < 10) {
        time_str += '0';
    }
    time_str += '' + time_ts.getDate() + ' ';


    if (time_ts.getHours() < 10) {
        time_str += '0';
    }
    time_str += '' + time_ts.getHours() + ':';

    if (time_ts.getMinutes() < 10) {
        time_str += '0';
    }
    time_str += '' + time_ts.getMinutes() + ':';

    if (time_ts.getSeconds() < 10) {
        time_str += '0';
    }
    time_str += '' + time_ts.getSeconds(); // + ':' + time_ts.getMilliseconds();

    return time_str;
}


exports.main = funcMain